﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemagoScript
{

    enum InstrumentHash {
        Guitar
    }

    class PlayInstrument : Goal
    {
        private DateTime startTime;
        private Prop instrumentProp;
        private InstrumentHash instrumentHash;

        public float SecondsToPlay { get; set; }

        public PlayInstrument(InstrumentHash instrumentHash, float secondsToPlay)
        {
            this.instrumentHash = instrumentHash;
            SecondsToPlay = secondsToPlay;
        }

        public override bool initialize()
        {
            if (!base.initialize())
            {
                return false;
            }

            startTime = DateTime.Now;

            startAnimation();

            return true;
        }

        public override bool update()
        {
            if (!base.update())
            {
                return false;
            }

            TimeSpan remainingTime = startTime.AddSeconds(SecondsToPlay) - DateTime.Now;
            if (remainingTime.TotalSeconds <= 0)
            {
                Game.Player.Character.Task.ClearAllImmediately();
                accomplish();
                return false;
            }
            else
            {
                if (remainingTime.TotalSeconds > 0)
                {
                    setGoalText("Attend que les spéctateurs aient assez apprécié la musique de Joe (Encore " + Tools.getTextFromTimespan(remainingTime) + ")");
                }
                else
                {
                    GTA.UI.ShowSubtitle("Tire toi de là ! On a appelé les flics !");
                }
            }

            return true;
        }

        private void startAnimation()
        {
            Ped player = Game.Player.Character;

            player.Task.ClearAllImmediately();

            if (instrumentProp == null)
            {
                if (instrumentHash == InstrumentHash.Guitar) { 
                    instrumentProp = World.CreateProp("prop_acc_guitar_01", player.Position + player.ForwardVector * 4.0f, true, true);
                    if (instrumentProp != null)
                    {
                        instrumentProp.AttachTo(player, 28252, new Vector3(-0.1f, 0.18f, 0.1f), new Vector3(180f, -120f, -10f));
                    }

                    player.Task.PlayAnimation("amb@world_human_musician@guitar@male@base", "base", 8f, -1, true, -1f);
                }
            }
        }

        public override void clear(bool removePhysicalElements = false)
        {
            if (instrumentProp != null && instrumentProp.Exists())
            {
                instrumentProp.Delete();
                instrumentProp = null;
            }            
        }
    }
}
