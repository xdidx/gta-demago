﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemagoScript
{
    class GoToPosition : Goal
    {
        private Vector3 destination;
        private Blip destinationBlip = null; 

        public GoToPosition(Vector3 position)
        {
            destination = position;
        }

        public override bool initialize()
        {
            if (!base.initialize())
            {
                return false;
            }
           
            if (destinationBlip != null)
            {
                destinationBlip.Remove();
            }
            createDestinationBlip();

            return true;
        }

        public void createDestinationBlip()
        {
            destinationBlip = World.CreateBlip(destination);
            destinationBlip.Sprite = BlipSprite.Crosshair;
            destinationBlip.Color = BlipColor.Green;
            destinationBlip.IsFlashing = true;
            destinationBlip.ShowRoute = true;
            destinationBlip.Position = destination;
        }

        public override bool update()
        {
            if (!base.update())
            {
                return false;
            }

            Ped player = Game.Player.Character;

            if (destination.DistanceTo(Game.Player.Character.Position) < 1.4)
            {
                destinationBlip.Remove();
                accomplish();
                return false;
            }
            else
            {
                if (destinationBlip == null)
                {
                    createDestinationBlip();
                }
                setGoalText("Rejoins l'endroit indiqué par le GPS");
            }

            return true;
        }

        public override void clear(bool removePhysicalElements = false)
        {
            if (destinationBlip != null && destinationBlip.Exists())
            {
                destinationBlip.Remove();
            }
        }
    }
}
