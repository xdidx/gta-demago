﻿using GTA;
using GTA.Math;
using GTA.Native;
using NativeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemagoScript
{
    class Joe : Mission
    {
        public static Vector3 joeHomePosition { get; } = new Vector3(2350f, 2529f, 46f);
        public static Vector3 firstSongPosition { get; } = new Vector3(2338f, 2547f, 48f); 
        public static Vector3 bikePositionAtHome { get; } = new Vector3(2353f, 2526f, 48f);
        public static Vector3 roadFaceToPoliceStationPosition { get; } = new Vector3(414f, -978f, 29f);
        public static Vector3 secondSongPosition { get; } = new Vector3(445f, -985f, 30f);
        public static Vector3 thirdSongBikePosition { get; } = new Vector3(681f, 565f, 128f);
        public static Vector3 thirdSongPosition { get; } = new Vector3(686f, 578f, 131f);

        private Vehicle bike;
        private List<Ped> spectatorsPeds = new List<Ped>();
        private float playerLife;
        private int playerLifeLastFrame;

        public override string getName()
        {
            return "Joe l'anticonformiste";
        }

        public override bool initialize()
        {
            if (!base.initialize())
            {
                return false;
            }

            World.Weather = Weather.Clear;
            
            Game.Player.Character.MaxHealth = 300;
            Game.Player.Character.Health = 300;

            Tools.TeleportPlayer(joeHomePosition);

            Ped player = Game.Player.Character;
            bike = World.CreateVehicle(VehicleHash.TriBike, bikePositionAtHome);
            bike.EnginePowerMultiplier = 5;
            bike.IsInvincible = true;

            List<PedHash> spectatorsHashes = new List<PedHash>();
            spectatorsHashes.Add(PedHash.Ashley);
            spectatorsHashes.Add(PedHash.Car3Guy2);
            spectatorsHashes.Add(PedHash.Car3Guy1);
            spectatorsHashes.Add(PedHash.Bankman);
            spectatorsHashes.Add(PedHash.Barry);
            spectatorsHashes.Add(PedHash.Beach01AFM);
            spectatorsHashes.Add(PedHash.Beach01AFY);
            spectatorsHashes.Add(PedHash.Beach01AMM);
            spectatorsHashes.Add(PedHash.Beach02AMM);
            
            foreach (PedHash hash in spectatorsHashes)
            {
                Ped ped = World.CreatePed(hash, firstSongPosition.Around(5));
                ped.Task.LookAt(firstSongPosition);
                if(ped != null && ped.Exists())
                { 
                    spectatorsPeds.Add(ped);
                }
            }
            
            //Game.PlayMusic();

            addGoal(new GoToPosition(firstSongPosition));

            Goal firstSongGoals = new PlayInstrument(InstrumentHash.Guitar, 20);
            addGoal(firstSongGoals);
            firstSongGoals.OnGoalStart += (sender) =>
            {
                foreach (Ped spectator in spectatorsPeds)
                {
                    spectator.Task.LookAt(player);
                    spectator.Task.PlayAnimation("facials@gen_male@variations@angry", "mood_angry_2", 8f, -1, true, -1f);
                    spectator.AlwaysKeepTask = true;
                }
            };

            firstSongGoals.OnGoalAccomplished += (sender, elaspedTime) =>
            {
                player.Armor = 100;
                player.Health = 300;
                GTA.UI.ShowSubtitle("Spectateurs : C'est nul ! Casse toi ! On a appelé les flics !");

                Game.Player.WantedLevel = 2;
            };
            
            GoToPositionInVehicle goToPoliceWithBikeGoal = new GoToPositionInVehicle(roadFaceToPoliceStationPosition, bike);
            addGoal(goToPoliceWithBikeGoal);
            goToPoliceWithBikeGoal.OnFirstTimeOnVehicle += (sender, vehicle) =>
            {
                //Tools.TeleportPlayer(roadFaceToPoliceStationPosition.Around(20));
            };

            addGoal(new GoToPosition(secondSongPosition));

            Goal secondSongGoals = new PlayInstrument(InstrumentHash.Guitar, 20);
            addGoal(secondSongGoals);
            secondSongGoals.OnGoalAccomplished += (sender, elaspedTime) =>
            {
                player.Armor = 100;
                player.Health = 300;
                GTA.UI.ShowSubtitle("Policier : Si tu te sors pas, c'est nous qui allons te faire sortir !");
                World.Weather = Weather.Clouds;
            };

            GoToPositionInVehicle goToTheaterWithBikeGoal = new GoToPositionInVehicle(thirdSongBikePosition, bike);
            addGoal(goToTheaterWithBikeGoal);
            goToTheaterWithBikeGoal.OnFirstTimeOnVehicle += (sender, vehicle) =>
            {
                player.Armor = 100;
                player.Health = 300;
                Game.Player.WantedLevel = 3;

                World.Weather = Weather.Raining;
            };
            

            addGoal(new GoToPosition(thirdSongPosition));

            Goal thirdSongGoals = new PlayInstrument(InstrumentHash.Guitar, 20);
            addGoal(thirdSongGoals);
            thirdSongGoals.OnGoalAccomplished += (sender, elaspedTime) =>
            {
                player.Armor = 100;
                player.Health = 300;
                Game.Player.WantedLevel = 4;

                World.Weather = Weather.ThunderStorm;

                GTA.UI.ShowSubtitle("Spectateurs : C'est nul ! Casse toi ! On a encore appelé les flics ! Tu vas avoir des problèmes !");
            };

            addGoal(new GoToPositionInVehicle(joeHomePosition, bike));

            return true;
        }

        public override void clear(bool removePhysicalElements = false)
        {
            base.clear(removePhysicalElements);

            Game.Player.Character.MaxHealth = 100;
            
            if (removePhysicalElements)
            {
                if (bike != null)
                {
                    bike.Delete();
                }
                foreach (Ped spectator in spectatorsPeds)
                {
                    if (spectator != null && spectator.Exists() && spectator.IsAlive)
                    {
                        spectator.Delete();
                    }
                }
                spectatorsPeds.Clear();
            }
        }

        public override bool update()
        {
            if (!base.update())
            {
                return false;
            }

            Ped player = Game.Player.Character;

            if (player.IsInVehicle() && player.CurrentVehicle == bike)
            {
                playerLife += 0.03f;
            }

            if (playerLifeLastFrame != player.Health)
            {
                playerLife = player.Health;
            }
            else
            {
                player.Health = (int)playerLife;
                playerLifeLastFrame = player.Health;
            }

            Game.Player.Character.Weapons.RemoveAll();
            
            return true;
        }

        public override void fillMenu(ref UIMenu menu)
        {
            base.fillMenu(ref menu);
        }
    }
}
