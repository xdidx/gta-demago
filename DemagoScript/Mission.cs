﻿using GTA;
using NativeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemagoScript
{
    abstract class Mission
    {
        public delegate void MissionStartEvent(Mission sender);
        public delegate void MissionFailEvent(Mission sender, string reason);
        public delegate void MissionAccomplishedEvent(Mission sender, TimeSpan elaspedTime);

        private List<Goal> goals = new List<Goal>();

        private bool active = false;
        private bool over = false;
        private bool initialized = false;
        private bool failed = false;
        private DateTime startMissionTime;

        /// <summary>
        /// Called when user start the mission.
        /// </summary>
        public event MissionStartEvent OnMissionStart;

        /// <summary>
        /// Called when all goals are accomplished
        /// </summary>
        public event MissionAccomplishedEvent OnMissionAccomplished;

        /// <summary>
        /// Called when a important event stop the mission
        /// </summary>
        public event MissionFailEvent OnMissionFail;

        public virtual bool initialize()
        {
            if (initialized)
            {
                return false;
            }

            clear(true);

            over = false;
            failed = false;
            initialized = true;

            return true;
        }

        public void start()
        {
            OnMissionStart?.Invoke(this);

            reset();
            initialize();
            active = true;

            startMissionTime = DateTime.Now;
        }

        public void reset()
        {
            initialized = false;
            foreach (Goal goal in goals)
            {
                goal.clear(false);
            }
        }

        public void stop(string reason = "")
        {
            if (active)
            {
                if (reason != "")
                {
                    GTA.UI.Notify("Mission arrêtée : " + reason);
                }
                active = false;
                reset();
            }
        }

        public virtual void accomplish()
        {
            TimeSpan elapsedTime = DateTime.Now - startMissionTime;
            OnMissionAccomplished?.Invoke(this, elapsedTime);
            active = false;
            over = true;

            clear(false);

            Game.Player.WantedLevel = 0;
            Game.Player.Character.Armor = 100;
            Game.Player.Character.Health = 100;
        }

        public bool isInProgress()
        {
            return active && !over && !failed;
        }

        public void fail(string reason = "")
        {
            if (active)
            {
                OnMissionFail?.Invoke(this, reason);
            }
            reset();
            failed = true;
            active = false;
            clear(false);
        }

        public void addGoal(Goal goal)
        {
            goal.OnGoalFail += (sender, reason) =>
            {
                fail(reason);
            };
            goals.Add(goal);
        }

        public virtual bool update()
        {
            initialize();

            if (!isInProgress())
            {
                active = false;
                return false;
            }

            if (Game.Player.Character.IsDead)
            {
                fail("Vous êtes mort");
                return false;
            }

            bool waitingGoals = false;
            foreach (Goal goal in goals)
            {
                if (!goal.isOver() && goal.update())
                {
                    waitingGoals = true;
                    break;
                }
                if (goal.isFailed())
                {
                    waitingGoals = true;
                    break;
                }
            }
            
            if (!waitingGoals)
            {
                accomplish();
            }

            return true;
        }

        abstract public string getName();

        public virtual void clear(bool removePhysicalElements = false)
        {
            foreach (Goal goal in goals)
            {
                goal.clear(removePhysicalElements);
            }
            if (removePhysicalElements)
            {
                goals.Clear();
            }
        }

        public virtual void fillMenu(ref UIMenu menu)
        {
            var startItem = new UIMenuItem("Démarrer la mission");
            menu.AddItem(startItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
                if (item == startItem)
                {
                    start();
                }
            };
        }
    }
}
