﻿using GTA;
using GTA.Math;
using GTA.Native;
using NativeUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemagoScript
{
    static class PlacesPositions
    {
        public static Vector3 GrooveStreet { get; } = new Vector3(110f, -1950f, 21.0f);
        public static Vector3 TaxiMission { get; } = new Vector3(-1495f, 4990f, 63f);
    }

    static class Tools
    {
        private static Vector3 lastPlayerPosition = Vector3.Zero;

        public static void update()
        {
            lastPlayerPosition = Game.Player.Character.Position;
        }

        public static bool playerMoved()
        {
            return lastPlayerPosition != Game.Player.Character.Position;
        }

        public static string getVehicleSpeedInKmh(Vehicle vehicle)
        {
            return getSpeedInKmh(vehicle.Speed).ToString() + "Km/h";
        }

        public static float getSpeedInKmh(float speed)
        {
            return (float)Math.Round(speed * 3);
        }
                
        /*TO IMPROVE => POSSIBLE BUG : GROUND DON'T LOAD QUICKLY ENOUGH ?*/
        private static Dictionary<Vector2, Vector3> savedGroundedPositions = new Dictionary<Vector2, Vector3>();
        public static Vector3 GetGroundedPosition(Vector3 position)
        {            
            bool groundFound = false;
            Vector2 position2d = new Vector2(position.X, position.Y);
            if (savedGroundedPositions.ContainsKey(position2d))
            {
                log("Tools.GetGroundedPosition() => Ground FOUND IN CACHE : x : " + position.X + " / y : " + position.Y + " / z : " + position.Z);
                return savedGroundedPositions[position2d];
            }
            
            Ped heightTestPed = World.CreatePed(PedHash.Abigail, Game.Player.Character.Position);
            if (heightTestPed == null || !heightTestPed.Exists())
            {
                return position;
            }

            for (int pedGroundHeight = 0; pedGroundHeight > 0; pedGroundHeight += 50)
            {
                position.Z = pedGroundHeight;
                heightTestPed.Position = position;

                Script.Wait(100);

                unsafe
                {
                    //But now, I have another issue of course ^^ If player is far from point on map, 
                    OutputArgument outArg = new OutputArgument();
                    if (Function.Call<bool>(Hash.GET_GROUND_Z_FOR_3D_COORD, position.X, position.Y, pedGroundHeight, outArg))
                    {
                        float output = outArg.GetResult<float>();
                        position.Z = output;
                        log("Tools.GetGroundedPosition() => Ground FOUND ! : x : " + position.X + " / y : " + position.Y + " / z : " + position.Z);
                        GTA.UI.Notify("Ground FOUND ! : x: " + position.X + " / y : " + position.Y + " / z : " + position.Z);
                        groundFound = true;
                        position.Z += 4.0f;
                        savedGroundedPositions.Add(new Vector2(position.X, position.Y), position);
                        break;
                    }
                    else
                    {
                        log("Tools.GetGroundedPosition() => Ground : x : " + position.X + " / y : " + position.Y + " / z : " + position.Z);
                    }
                }
            }
            heightTestPed.Delete();

            if (!groundFound)
            {
                log("Tools.GetGroundedPosition() => Ground NOT FOUND : x : " + position.X + " / y : " + position.Y + " / z : " + position.Z);
                //position.Z = 50;
            }

            return position;
        }

        public static void TeleportPlayer(Vector3 position, bool withVehicle = true)
        {
            Entity entityToTeleport = Game.Player.Character;
            if (withVehicle && Game.Player.Character.IsInVehicle())
            {
                entityToTeleport = Game.Player.Character.CurrentVehicle;
            }
            entityToTeleport.Position = position;
            entityToTeleport.Velocity = Vector3.Zero;
            entityToTeleport.Rotation = Vector3.Zero;
        }

        public static void GiveParachuteToPlayer()
        {
            Function.Call(Hash.GIVE_WEAPON_TO_PED, Game.Player.Character, Function.Call<int>(Hash.GET_HASH_KEY, "gadget_parachute"), 1, 0, 0);
        }

        public static string getTextFromTimespan(TimeSpan time)
        {
            var totalTime = "";
            if (time.Minutes > 0)
            {
                totalTime += " "+time.Minutes + " minute";
                if (time.Minutes > 1)
                {
                    totalTime += "s";
                }
            }
            if (time.Seconds > 0)
            {
                if (totalTime != "")
                {
                    totalTime += " et ";
                }

                totalTime += time.Seconds + " seconde";

                if (time.Seconds > 1)
                {
                    totalTime += "s";
                }
            }
            return totalTime;
        }

        public static Vector3 GetSafeRoadPos(Vector3 originalPos)
        {
            OutputArgument outArg = new OutputArgument();
            Function.Call<int>(Hash.GET_CLOSEST_VEHICLE_NODE, originalPos.X, originalPos.Y, originalPos.Z, outArg, 1, 1077936128, 0);
            Vector3 output = outArg.GetResult<Vector3>();
            if (output != Vector3.Zero)
            {
                output.Z += 3;
                return output;
            }
            return originalPos;
        }

        public static void log(string message, Vector3 position)
        {
            log(message + " - X : " + position.X + " / Y : " + position.Y + " - Z : " + position.Z);
        }

        public static void log(string message)
        {
            using (StreamWriter logStreamWriter = new StreamWriter("C:/Program Files/Rockstar Games/Grand Theft Auto V/scripts/test.log", true))
            {
                try
                {
                    logStreamWriter.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "]" + message);
                }
                finally
                {
                    logStreamWriter.Close();
                }
            }
        }
    }
}
