﻿using GTA;
using GTA.Native;
using NativeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemagoScript
{
    class DemagoMenu
    {
        private MenuPool menuPool;
        private UIMenu mainMenu;

        public delegate void MenuAction();
        
        public DemagoMenu()
        {
            menuPool = new MenuPool();
            mainMenu = new UIMenu("GTA Demago", "~b~Configuration du mod");
            menuPool.Add(mainMenu);
        }

        public void addMissionMenu(Mission mission)
        {
            var missionMenu = menuPool.AddSubMenu(mainMenu, mission.getName());
            mission.fillMenu(ref missionMenu);
        }



        public UIMenu addSubmenu(string menuName)
        {
            var missionMenu = menuPool.AddSubMenu(mainMenu, menuName);
            return missionMenu;
        }

        public void process()
        {
            menuPool.ProcessMenus();
        }

        public UIMenu getMainMenu()
        {
            return mainMenu;
        }

        public void show()
        {
            mainMenu.Visible = true;
        }

        public void hide()
        {
            mainMenu.Visible = false;
        }

        public void toggleDisplay()
        {
            mainMenu.Visible = !mainMenu.Visible;
        }

    }
}
