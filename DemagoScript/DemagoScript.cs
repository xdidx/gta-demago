﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GTA;
using GTA.Native;
using GTA.Math;
using NativeUI;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace DemagoScript
{
    public class DemagoScript : Script
    {
        private List<Mission> missions = null;
        private DemagoMenu menu;
        private bool zeroGravity = false;
        private bool initialized = false;

        private Vector3 teleportationPosition = Joe.joeHomePosition;
        
        public DemagoScript()
        {
            Tools.log("-------------Initialisation du mod GTA Démago------------");

            Tick += OnTick;
            KeyDown += OnKeyDown;
        }

        private void initialize()
        {
            if (initialized)
            {
                return;
            }

            createMenu();
            initialized = true;
        }

        void OnTick(object sender, EventArgs e)
        {            
            initialize();

            menu.process();
            var missions = getMissions();
            foreach (Mission mission in missions)
            {
                if (mission.isInProgress())
                {
                    mission.update();
                }
            }

            Tools.update();
        }

        void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                menu.toggleDisplay();
            }
        }

        private void createMenu()
        {
            menu = new DemagoMenu();

            var missions = getMissions();
            foreach (Mission mission in missions)
            {
                menu.addMissionMenu(mission);
            }

            var gravityActiveItem = new UIMenuCheckboxItem("Zéro gravité", zeroGravity, "Si la case est cochée, il n'y aura plus de gravité sur la map entière");
            menu.getMainMenu().AddItem(gravityActiveItem);
            menu.getMainMenu().OnCheckboxChange += (sender, item, checked_) =>
            {
                if (item == gravityActiveItem)
                {
                    zeroGravity = checked_;
                    if (zeroGravity)
                    {
                        Function.Call(Hash.SET_GRAVITY_LEVEL, 3);
                    }
                    else
                    {
                        Function.Call(Hash.SET_GRAVITY_LEVEL, 0);
                    }
                }
            };

            var teleportToTaxiItem = new UIMenuItem("Se téléporter à la mission taxi");
            menu.getMainMenu().AddItem(teleportToTaxiItem);

            menu.getMainMenu().OnItemSelect += (sender, item, index) =>
            {
                if (item == teleportToTaxiItem)
                {
                    Game.Player.Character.Position = PlacesPositions.TaxiMission;
                }
            };

            var wantedLevelItem = new UIMenuItem("Baisser l'indice de recherche");
            var spawnCarItem = new UIMenuItem("Faire apparaitre une voiture aléatoire");
            var spawnNiceCarItem = new UIMenuItem("Faire apparaitre une voiture rapide");
            var healPlayerItem = new UIMenuItem("Se soigner et réparer la voiture");
            var showPositionItem = new UIMenuItem("Afficher la position");
            var showRotationItem = new UIMenuItem("Afficher la rotation");
            var teleportItem = new UIMenuItem("Se téléporter");
            var roadTeleportItem = new UIMenuItem("Se téléporter sur la route");
            var safeTeleportItem = new UIMenuItem("Se téléporter sur le sol", "La fonctionnalité est en cours de développement");

            var toolsMenu = menu.addSubmenu("Outils");
            toolsMenu.AddItem(wantedLevelItem);
            toolsMenu.AddItem(spawnCarItem);
            toolsMenu.AddItem(spawnNiceCarItem);
            toolsMenu.AddItem(healPlayerItem);
            toolsMenu.AddItem(showPositionItem);
            toolsMenu.AddItem(showRotationItem);
            toolsMenu.AddItem(teleportItem);
            toolsMenu.AddItem(roadTeleportItem);
            toolsMenu.AddItem(safeTeleportItem);

            toolsMenu.OnItemSelect += (sender, item, index) =>
            {
                if (item == roadTeleportItem)
                {
                    Tools.TeleportPlayer(Tools.GetSafeRoadPos(teleportationPosition));
                }
                if (item == safeTeleportItem)
                {
                    /*Tools.GetGroundedPosition() HAVE TO BE UPDATE*/
                    Tools.TeleportPlayer(Tools.GetGroundedPosition(teleportationPosition));
                }
                if (item == teleportItem)
                {
                    Tools.TeleportPlayer(teleportationPosition);
                }
                if (item == teleportToTaxiItem)
                {
                    Game.Player.Character.Position = PlacesPositions.TaxiMission;
                }
                if (item == showPositionItem)
                {
                    GTA.UI.Notify("player X : " + Game.Player.Character.Position.X + " / Y : " + Game.Player.Character.Position.Y + " / Z : " + Game.Player.Character.Position.Z);
                }
                if (item == showRotationItem)
                {
                    GTA.UI.Notify("rot X : " + Game.Player.Character.Rotation.X + " / Y : " + Game.Player.Character.Rotation.Y + " / Z : " + Game.Player.Character.Rotation.Z);
                }
                if (item == wantedLevelItem)
                {
                    Game.Player.WantedLevel = 0;
                }
                if (item == spawnCarItem)
                {
                    Array vehicleValues = Enum.GetValues(typeof(VehicleHash));
                    Random random = new Random();
                    VehicleHash randomValue = (VehicleHash)vehicleValues.GetValue(random.Next(vehicleValues.Length));
                    World.CreateVehicle(randomValue, Tools.GetSafeRoadPos(Game.Player.Character.Position));
                }
                if (item == spawnNiceCarItem)
                {
                    World.CreateVehicle(VehicleHash.T20, Tools.GetSafeRoadPos(Game.Player.Character.Position));
                }
                if (item == healPlayerItem)
                {
                    Game.Player.Character.Health = Game.Player.Character.MaxHealth;
                    Game.Player.Character.Armor = 100;
                    if (Game.Player.Character.IsInVehicle())
                    {
                        Game.Player.Character.CurrentVehicle.Repair();
                    }
                }
            };
            
            var xItem = new UIMenuEditableNumericItem("X", teleportationPosition.X, -8000, 8000, 1);
            var yItem = new UIMenuEditableNumericItem("Y", teleportationPosition.Y, -8000, 8000, 1);
            var zItem = new UIMenuEditableNumericItem("Z", teleportationPosition.Z, -8000, 8000, 1);

            toolsMenu.AddItem(xItem);
            toolsMenu.AddItem(yItem);
            toolsMenu.AddItem(zItem);

            toolsMenu.OnValueChange += (sender, item, value) =>
            {
                if (item == xItem)
                {
                    teleportationPosition.X = value;
                }
                if (item == yItem)
                {
                    teleportationPosition.Y = value;
                }
                if (item == zItem)
                {
                    teleportationPosition.Z = value;
                }
            };
        }

        private List<Mission> getMissions()
        {
            if (missions == null)
            {
                missions = new List<Mission>();

                Type[] missionsClassesList = Assembly.GetExecutingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(Mission))).ToArray();
                foreach (Type missionClass in missionsClassesList)
                {
                    Mission newMission = (Mission)Activator.CreateInstance(missionClass);

                    newMission.OnMissionStart += (sender) =>
                    {
                        foreach (Mission mission in missions)
                        {
                            mission.stop("Une autre mission a été démarrée");
                        }
                        GTA.UI.Notify(sender.getName());
                    };

                    newMission.OnMissionAccomplished += (sender, time) =>
                    {
                        var successMessage = sender.getName() + " : Mission accomplie";
                        if (Tools.getTextFromTimespan(time) != "")
                        {
                            successMessage += " en " + Tools.getTextFromTimespan(time);
                        }
                        GTA.UI.Notify(successMessage);
                    };

                    newMission.OnMissionFail += (sender, reason) =>
                    {
                        GTA.UI.Notify("La mission a échouée : " + reason);
                    };

                    missions.Add(newMission);
                }
            }

            return missions;
        }
        
    }
}